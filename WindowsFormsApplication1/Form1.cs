﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YTUBER;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        //переходим по 1 ссылке, берем ссылку и переходим по ней.
        //ждем указаное время, проверяем и радуемся
        string _checkLink = "https://ytuber.ru/yt/check";
        string _openLink = "https://ytuber.ru/yt/open";
        string _viewLink = "https://ytuber.ru/yt/view";//просмотры ютуба
        Queue<task> _tasks = new Queue<task>();
        BindingSource _source=new BindingSource();
        Timer _timer = new Timer();
        private TaskImplementation _taskImplementation;

        private task _tempTask = new task();
        
        public Form1()
        {
            InitializeComponent();

            _taskImplementation = new TaskImplementation(_openLink, _checkLink);
            _taskImplementation.OnComplete += TP_OnComplete;

            _timer.Interval = 1000;
            _timer.Tick += timer_Tick;
            _timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (_tempTask.id != 0)
            {
                _tempTask.seconds--;
                if (_tempTask.seconds < -1)
                    _taskImplementation.CheckTask(_tempTask.id);
            }
        }

        private void StartImplementation_Click(object sender, EventArgs e)
        {
            Go();
        }

        void TP_OnComplete(int id, string message = null, Response response = null)
        {
            addLog(message, _tempTask);
            Go();
        }

        private void gridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            Color color = new Color();
            color = Color.White;

        }

        private void addLog(string Description, task temp=null)
        {
            DataGridViewRow row = new DataGridViewRow();

            DataGridViewCell id = new DataGridViewTextBoxCell();
            DataGridViewCell description = new DataGridViewTextBoxCell();
            DataGridViewCell points = new DataGridViewTextBoxCell();
            description.Value = Description;
            if (temp != null)
            {
                id.Value = temp.id;
                points.Value = temp.points;
            }
            else
            {
                id.Value = String.Empty;
                points.Value = String.Empty;
            }
            row.Cells.AddRange(id, description, points);

            LogsView.Rows.Add(row);
        }

        /// <summary>
        /// Начинает выполнение задания
        /// </summary>
        private void Go()
        {
            do
            {
                if (_tasks.Count>0)
                    _tempTask = _tasks.Dequeue();
            } while (_tempTask.value != YTUBER.enums.taskValue.wait && _tasks.Count > 0);
            
            if (_tasks.Count>=0)
                _taskImplementation.StartNextTask(_tempTask.id);
            else
            {
                _timer.Stop();
                addLog("Все задачи выполнены");
            }
        }

        private void load_Click(object sender, EventArgs e)
        {
            var temp = new LoadTasks().Loading(_viewLink, 10);
            foreach (var t in temp)
                _tasks.Enqueue(t);

            _source.DataSource = _tasks;
            gridView.DataSource = _source;

            Load.Enabled = false;
        }
    }
}
