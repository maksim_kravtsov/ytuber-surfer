﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridView = new System.Windows.Forms.DataGridView();
            this.Load = new System.Windows.Forms.Button();
            this.LogsView = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartImplementation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogsView)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView
            // 
            this.gridView.AllowUserToAddRows = false;
            this.gridView.AllowUserToDeleteRows = false;
            this.gridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.gridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.gridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.gridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridView.Location = new System.Drawing.Point(0, 68);
            this.gridView.Name = "gridView";
            this.gridView.ReadOnly = true;
            this.gridView.RowHeadersVisible = false;
            this.gridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridView.Size = new System.Drawing.Size(767, 184);
            this.gridView.TabIndex = 0;
            this.gridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.gridView_RowsAdded);
            // 
            // Load
            // 
            this.Load.Dock = System.Windows.Forms.DockStyle.Top;
            this.Load.Location = new System.Drawing.Point(0, 34);
            this.Load.Name = "Load";
            this.Load.Size = new System.Drawing.Size(767, 34);
            this.Load.TabIndex = 1;
            this.Load.Text = "Загрузить";
            this.Load.UseVisualStyleBackColor = true;
            this.Load.Click += new System.EventHandler(this.load_Click);
            // 
            // LogsView
            // 
            this.LogsView.AllowUserToAddRows = false;
            this.LogsView.AllowUserToDeleteRows = false;
            this.LogsView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LogsView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Description,
            this.Price});
            this.LogsView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LogsView.Location = new System.Drawing.Point(0, 252);
            this.LogsView.Name = "LogsView";
            this.LogsView.ReadOnly = true;
            this.LogsView.Size = new System.Drawing.Size(767, 155);
            this.LogsView.TabIndex = 2;
            // 
            // Id
            // 
            this.Id.HeaderText = "id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 60;
            // 
            // Description
            // 
            this.Description.HeaderText = "описание";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Width = 600;
            // 
            // Price
            // 
            this.Price.HeaderText = "Ценник";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 60;
            // 
            // StartImplementation
            // 
            this.StartImplementation.Dock = System.Windows.Forms.DockStyle.Top;
            this.StartImplementation.Location = new System.Drawing.Point(0, 0);
            this.StartImplementation.Name = "StartImplementation";
            this.StartImplementation.Size = new System.Drawing.Size(767, 34);
            this.StartImplementation.TabIndex = 3;
            this.StartImplementation.Text = "Начать выполнение";
            this.StartImplementation.UseVisualStyleBackColor = true;
            this.StartImplementation.Click += new System.EventHandler(this.StartImplementation_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 407);
            this.Controls.Add(this.gridView);
            this.Controls.Add(this.LogsView);
            this.Controls.Add(this.Load);
            this.Controls.Add(this.StartImplementation);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogsView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridView;
        private System.Windows.Forms.Button Load;
        private System.Windows.Forms.DataGridView LogsView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.Button StartImplementation;

    }
}

