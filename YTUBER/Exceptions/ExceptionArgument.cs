﻿using System;
using System.Runtime.Serialization;

namespace YTUBER.Exceptions
{
    [Serializable]
    class ExceptionArgument : Exception
    {
        public ExceptionArgument() { }

        public ExceptionArgument(string message) : base(message) { }

        public ExceptionArgument(string message, Exception inner) : base(message, inner) { }

        public ExceptionArgument(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
