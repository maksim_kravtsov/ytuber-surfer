﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
namespace YTUBER
{
    public class LoadTasks
    {

        /// <summary>
        /// Получение задач на выполнение
        /// </summary>
        /// <param name="link">ссылка на страницу с задачами</param>
        /// <param name="count">количество страниц, с которых будут взяты задачи</param>
        /// <param name="start">смещение страниц от нуля</param>
        /// <returns></returns>
        public List<task> Loading(string link, int count=2, int start = 0)
        {
            if (count < 0 || start < 0) 
                throw new Exceptions.ExceptionArgument("Один или несколько аргументов имеют недопустимые значения");

            var tasks = new List<task>();
            using (var browser = new WebBrowser())
            {
                for (int i = start; i < count; i++)
                {
                    browser.Navigate(String.Format("{0}/{1}", link, i * 12));
                    while (browser.ReadyState != System.Windows.Forms.WebBrowserReadyState.Complete)
                        Application.DoEvents();

                    tasks.AddRange(ReadTasks(browser.DocumentText));
                }
            }
            return tasks;
        }

        /// <summary>
        /// Получение информации о задачах
        /// </summary>
        /// <param name="html">html докумнт</param>
        /// <returns></returns>
        private List<task> ReadTasks(string html)
        {
            List<task> result = new List<task>();
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            foreach (var row in doc.DocumentNode.SelectNodes("//*[@id=\"page-wrapper\"]/div/div/div/table/tbody/tr"))
            {
                result.Add(ReadTaskInfo(row));
            }
            return result;
        }

        /// <summary>
        /// Полчуение информации о задаче
        /// </summary>
        /// <param name="row"></param>
        private task ReadTaskInfo(HtmlNode row)
        {
            task temp = new task();
            if (row.GetAttributeValue("class", null) == "done")
                temp.value = enums.taskValue.done;

            var match = Regex.Match(row.ChildNodes[7].InnerText, @"[0-9].[0-9]+(?:\.[0-9]*)?");
            if (match.Success)
                temp.points = Convert.ToDouble(match.Value.Replace('.', ','));

            foreach (var td in row.ChildNodes)
            {
                string type = td.GetAttributeValue("class", null);

                if (type == null)
                    continue;

                switch (type)
                {
                    case "id":
                        temp.id = Convert.ToInt32(td.InnerText);
                        break;
                    case "time":
                        temp.seconds = Convert.ToInt32(td.InnerText);
                        break;
                }
            }
            return temp;
        }

    }
}
