﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace YTUBER
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Response
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("user_balance")]
        public string User_balance { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("href")]
        public string Link { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }

        public void Deserialize(string json)
        {
            try
            {
                Newtonsoft.Json.Linq.JObject obj = Newtonsoft.Json.Linq.JObject.Parse(json);
                ToThis(JsonConvert.DeserializeObject<Response>(obj.ToString()));
            }
            catch
            {
                throw new Exceptions.ExceptionJsonDeserialize(message: "Ошибка: [" + json +"]");
            }
        }

        private void ToThis(Response resp)
        {
            this.Error = resp.Error;
            this.Link = resp.Link;
            this.Location = resp.Location;
            this.Status = resp.Status;
            this.Text = resp.Text;
            this.User_balance = resp.User_balance;
        }
    }
}
